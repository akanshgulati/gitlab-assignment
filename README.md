# gitlab-assignment

**Q1. What is the benefit of using a library like React/Vue/Angular vs using plain old JavaScript? What is your experience level with one of those frameworks?**

A: On the basis of using Angular and Vue, here are the pros of the modern framework over native Javascript: 
##### Benefits
1. **Sync between model and view**
With frameworks, it’s easy to keep model and view in sync for an application.  
    - If the application has frequently updating HTML(one-way binding) be it a string, an HTML attribute property name, HTML attribute property value, it is fast and easy with JS frameworks.
    e.g. In Vue, if you set `name` as Gitlab and use below interpolation technique in HTML, it will be rendered like below:
        ````html
            <div>{{name}}</div> -> <div>Gitlab</div>
        ````
    - If the application needs updates on the model from the HTML elements(two-way binding), frameworks can easily leverage that functionality.
    e.g. the below syntax in Vue can keep value of name updated in view and model
        ````html
        <input v-model="name"/> 
        ````
2. **Component-based development**
With frameworks, styles, template and functionalities can be combined into a single file to make separate components for DOM. These components are generic enough to be reused in the application for different use cases.
e.g. a `button` can be of multiple types like *primary, secondary, large and small*, we can use various props in Vue for the configuration of a button and render it as required. 
3. **Data consistency is better**
With complex functionalities on the frontend and maintaining the state of the data in different parts of the application is important. These frameworks provide special libraries(Redux and Vuex) for maintaining the states. This makes the debugging also easy.
e.g. while debugging an error like data manipulation due to being used in different part of the application, a developer could not find the real place of issue and due to this debugging becomes cumbersome. Vuex makes it easy by attaching every detail which could be used in debugging. 
4. ***Huge community support and time-saving***
Having a large list of contributors to these frameworks, many complex problems are solved within the frameworks, e.g. use of Virtual DOM, routing modules are already present, etc. which are constantly updated with W3C guidelines, reducing organisation cost on maintenance of separate frameworks of their own. Any newcomer can be easily on-boarded due to similar frameworks in different organisation. 

##### Experience
1. I have worked on Angular 1.x version for 3 years and build many scalable and generic components, multi-nested directives, etc. At the current organization, we are handling state management by building our own framework on the principles of Ember.js.
2. I have used ui-router for routing part of SPA application and handling data and among 5-6 level nested directives for complex functionalities. 
3. Working on lazily including components as per the conditions.
4. Worked on developing a UI kit in Vue for improving the re-usability for upcoming projects.
___
**Q2. When is CORS needed and how does it work in the browser?**

CORS, i.e. Cross-Origin Resource Sharing, a security policy, is essential for requesting data from a different origin(other domain, other HTTP protocol or other port). Generally, an application has its assets (files like stylesheets, images, fonts) hosted on different servers or applications use third-party resources, these increase the risk of malpractice. So, CORS enables servers to limit the access and define how the resources can be accessed from it on the client side.
e.g. if you order something from Amazon, the security guard of the society comes in between and verifies before letting the delivery guy inside the society but the same guard does not checks if you lend something from your neighbour. 

##### CORS in browser
In browsers, CORS is handled with the help of headers which are passed back and forth in request and response. Few of the CORS headers are: 

- Access-Control-Allow-Origin 
- Access-Control-Allow-Credentials
- Access-Control-Allow-Headers
- Access-Control-Allow-Methods
- Access-Control-Expose-Headers
- Access-Control-Max-Age
- Access-Control-Request-Headers
- Access-Control-Request-Method
- Origin

When the client requests for a resource, the browser detects the request is being made to a different origin. It sends a pre-flight request with `OPTIONS` method and checks if the request made by the client is safe.
e.g. let's assume that the client wants to make DELETE method request to a different origin, the flow would be:

![alt Flow of CORS for DELETE method][cors]
___
**Q3. What is an XSS attack? Explain what can lead to one and how can it be prevented?**

XSS, Cross Site Scripting attack, is a client-side attack in which a malicious script is injected on the website and when any user visits the same web page, the script is executed on user's browser. So, it is a technique to deliver malicious code through a legitimate web page. 
The malicious code could possibly do the following: 
- Send XHR calls to an external resource and send user's private data (accessible by web pages like geolocation, session data, etc.) along with the call.
- Update the DOM of the web page for a phishing attack.

An XSS attack is possible by form elements present on the web page, the input data is saveable and accessible by other users. So, in order to prevent it we can validate the data before saving it: 
- Sanitize the content coming from a user before saving it. Sanitization, as the word conveys, is a technique by which all the keywords which could cause XSS are removed or escaped. 

e.g. if a user posts a comment having `<script>` tag on the web page, we can convert the `>` to `&#60` symbol and `<` to `&#62` symbol to make it non-executable once comment is posted.

___
**Q4. Tell us about your latest "hard to debug" problem. How did you resolve it? Which tools did you use?**

A. There was an year old bug in one of the modules of application which use to come randomly and it has a huge impact on customers unsaved data. There were no proper reproducible steps for the bug and we needed to find the issue causing it. We planned an entire day for it (it's important to be fixed) when the entire team will try individually to reproduce it or red flag the code that can cause it. 

1. As we are using `Sentry` for logging our application error, I tried to look for the errors that were logged in Sentry for the users who reported the issue. 
2. I filtered the errors that could possibly impact the module in which issue was there
3. Then, I used the `stack trace` and  list of `breadcrumbs`(elements which user clicks on application) which Sentry provides to understand the user journey before the issue.
4. I used Quicktime `screen recording` feature in order to keep a recording of the steps which I followed to easily retrace the path. 
5. Fortunately, I was able to reach to the bug after 3-4 hours of effort, selected different errors logged in Sentry. 

The screen recording then helped in reproducing the error on other's machine too and finally, we were able to figure out the code snippet which was causing it. So, the error was related to a field set on `$rootScope`(Angular) which was not updated for a particular flow. Here is the screenshot of the appreciation I received. 
![alt Appreciation for solving bug][error-solving-appreciation]

**Q5. Tell us about your most advanced/exciting/mind-blowing JS/CSS implementation**

The recent exciting implementation of mine in Javascript was while working on a feature to which let user re-arrange and/or add new elements on page, part of [VWO Editor](https://vwo.com/knowledge/how-to-use-visual-editor-to-create-variations/). It required advanced knowledge of Web DOM API and mathematics for following: 
1. Calculate how to select the element on the page(reference element) to add/re-arrange element on the page and position(before, left, right and after) with respect to the element where the new element will be inserted. Learnt about new APIs like `document.elementFromPoint`
e.g. the green border depicts where to place the element.
![alt rearrange operation][rearrange]

2. Learnt about `CSSSupports` API in order to validate what CSS property value can be applied to which CSS property field while looking for best way to let user apply CSS 



[error-solving-appreciation]: ./assets/editor-day.png
[cors]: ./assets/cors-flow.jpg
[rearrange]: ./assets/re-arrange%20operation.jpg
